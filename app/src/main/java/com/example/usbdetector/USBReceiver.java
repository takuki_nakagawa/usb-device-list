package com.example.usbdetector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import static com.example.usbdetector.MainActivity.*;

public class USBReceiver extends BroadcastReceiver {

    private static final String TAG = "USBReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive, " + intent.getAction().toString() + ", " + intent.getExtras().getBoolean("connected"));
        Log.d(TAG, "onReceive, " + intent.getAction().toString() + ", " + intent.getExtras().getBoolean("host_connected"));
        Log.d(TAG, "onReceive, " + intent.getAction().toString() + ", " + intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY));//null
        Log.d(TAG, "onReceive, " + intent.getAction().toString() + ", " + intent.getStringExtra("configuration"));//null
        Log.d(TAG, "onReceive, " + intent.getAction().toString() + ", " + intent.getExtras().toString());
        //adb Bundle[{host_connected=false, connected=true, unlocked=true, adb=true, mtp=true, configured=true}
        //usb memory Bundle[{host_connected=true, connected=false, unlocked=true, adb=true, mtp=true, configured=false}]
        //usb mouse Bundle[{host_connected=true, connected=false, unlocked=true, adb=true, mtp=true, configured=false}]
        MainActivity.showUSBList(intent);
    }

}
